// **************************************************** constructor functions *******************************************************
function constructor_function(first_name,last_name)
{   // this = {}
    this.first_name = first_name;
    this.last_name = last_name;
    this.fimplementation = function()
    {
        console.log(`full name is:${this.first_name} ${this.last_name}`);
    }
    //return {}; // return this newly created object
}
let object1 = new constructor_function("aaa","bb");
let object2 = new constructor_function("ccc","dd");

console.log(object1);
console.log(object2);
//**************************************************** function literals ************************************************************

let function_literal = function ()
{
    console.log("this is a function literal name of this function can be given but it will not have any significance");
}
