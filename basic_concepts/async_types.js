var async = require('async');

  // \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ async.series \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

// When we have to run multiple tasks which depend on the output of 
// the previous task, series comes to our rescue.

async.series({
    1: function(callback) {
      setTimeout(function() {
        console.log('Task 1');
        callback(null, 'one');
      }, 200);
    },
    2: function(callback) {
      setTimeout(function() {
        console.log('Task 2');
        callback(null, 'two');
      }, 300);
    },
    3: function(callback) {
      setTimeout(function() {
        console.log('Task 3');
        callback(null, 'three');
      }, 100);
    }
  },
  function(err, results) {
    console.log(results);
    // results is now equal to: { 1: 'one', 2: 'two', 3:'three' }
  });


  // \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ async.paralell \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

  // When we have to run multiple tasks independent of each other without waiting 
  // until the previous task has completed, parallel comes into the picture.

// async.parallel([
//     function(callback) {
//       setTimeout(function() {
//         console.log('Task One');
//         callback(null, 1);
//       }, 200);
//     },
//     function(callback) {
//       setTimeout(function() {
//         console.log('Task Two');
//         callback(null, 2);
//       }, 100);
//     }
//   ],
//   function(err, results) {
//     console.log(results);
//     // the results array will equal [1, 2] even though
//     // the second function had a shorter timeout.
//   });