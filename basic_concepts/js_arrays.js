// *******************************   array constructors
//The constructor property returns the function that created the Array prototype.
// const fruits = ["Banana", "Orange", "Apple", "Mango"];
// let text = fruits.constructor;
// console.log("array_constructor",text);

// ******************************* prototypes
//prototype allows you to add new properties and methods to arrays.prototype is a property available with all JavaScript objects.
// You should not change the prototype of built in JavaScript datatypes like:

// --- example one ---
// Array.prototype.myUcase = function() {
//     for (let i = 0; i < this.length; i++) {
//       this[i] = this[i].toUpperCase();
//     }
//   };

// var fruits = ["Banana", "Orange", "Apple", "Mango"];
// console.log("before",fruits);
// fruits.myUcase();
// console.log("after",fruits)

// // --- example two ---  
// function Person(first, last, age, eyecolor) {
//     this.firstName = first;
//     this.lastName = last;
//     this.eyeColor = eyecolor;
//   }
  
// let obj = new Person('a','b','3','c');
// console.log("before",obj.nationality);
// Person.prototype.nationality = "English";
//     obj = new Person('a','b','3','c');
// console.log("after",obj.nationality);

// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ array methods ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
// ******************************* concat()
// The concat() method concatenates (joins) two or more arrays.
// The concat() method returns a new array, containing the joined arrays.

// const arr1 = ["Cecilie", "Lone"];
// const arr2 = ["Emil", "Tobias", "Linus"];
// const arr3 = ["Robin"];
// const children = arr1.concat(arr2,arr3);
// console.log(children);

// ****************************** every()
// The every() method executes a function for each array element.
// The every() method returns true if the function returns true for all elements.
// The every() method returns false if the function returns false for one element.
// The every() method does not execute the function for empty elements.
// The every() method does not change the original array.

// const survey = [
//     { name: "Steve", answer: "Yes"},
//     { name: "Jessica", answer: "Yes"},
//     { name: "Peter", answer: "Yes"},
//     { name: "Elaine", answer: "No"}
//   ];
  
//   let result = survey.every(isSameAnswer);
  
//   function isSameAnswer(el, index, arr) {
//     console.log("this is index",index);
//     if (index === 0) {
//       return true;
//     } else {
//       return (el.answer === arr[index - 1].answer);
//     }
//   }
//   console.log(result);

// ****************************** map()
//   map() creates a new array from calling a function for every array element.
//   map() calls a function once for each element in an array.
//   map() does not execute the function for empty elements.
//   map() does not change the original array.

// var officers = [
//     { id: 20, name: 'Captain Piett' },
//     { id: 24, name: 'General Veers' },
//     { id: 56, name: 'Admiral Ozzel' },
//     { id: 88, name: 'Commander Jerjerrod' }
//   ];

//   var officersIds = officers.map(function (officer) {
//     return officer.id
//   });
//   //     or
//   const officersIds = officers.map(officer => officer.id);

// **************************** reduce()
// Just like .map(), .reduce() also runs a callback for each element of an array. What’s different here is that reduce passes the result of this callback (the accumulator) from one array element to the other.
// The accumulator can be pretty much anything (integer, string, object, etc.) and must be instantiated or passed when calling .reduce().
// var pilots = [
//     {
//       id: 10,
//       name: "Poe Dameron",
//       years: 14,
//     },
//     {
//       id: 2,
//       name: "Temmin 'Snap' Wexley",
//       years: 30,
//     },
//     {
//       id: 41,
//       name: "Tallissan Lintra",
//       years: 16,
//     },
//     {
//       id: 99,
//       name: "Ello Asty",
//       years: 22,
//     }
//   ];
//   var totalYears = pilots.reduce(function (accumulator, pilot) {
//     return accumulator + pilot.years;
//   }, 0);
//   console.log("total years",totalYears);

// *************************************** filter()
//Basically, if the callback function returns true, the current element will be in the resulting array. If it returns false, it won’t be.
// var pilots = [
//     {
//       id: 2,
//       name: "Wedge Antilles",
//       faction: "Rebels",
//     },
//     {
//       id: 8,
//       name: "Ciena Ree",
//       faction: "Empire",
//     },
//     {
//       id: 40,
//       name: "Iden Versio",
//       faction: "Empire",
//     },
//     {
//       id: 66,
//       name: "Thane Kyrell",
//       faction: "Rebels",
//     }
//   ];

// var rebels = pilots.filter(function (pilot) {
//     return pilot.faction === "Rebels";
//   });
//   var empire = pilots.filter(function (pilot) {
//     return pilot.faction === "Empire";
//   });

// ********************************************* foreach()
// The forEach() method calls a function for each element in an array.
// The forEach() method is not executed for empty elements.

// const numbers = [65, 44, 12, 4];
// console.log("before",numbers);
// numbers.forEach(myFunction)
// function myFunction(item, index, arr) {
//   arr[index] = item * 10;
// }
// console.log("after",numbers);

// ********************************************* indexOf()   
// The indexOf() method returns the first index (position) of a specified value.
// The indexOf() method returns -1 if the value is not found.
// The indexOf() method starts at a specified index and searches from left to right.

// const fruits = ["Banana", "Orange", "Apple", "Mango"];
// let index = fruits.indexOf("Apple");
// or
// const fruits = ["Banana", "Orange", "Apple", "Mango", "Apple"];
// let index = fruits.indexOf("Apple", 3);
// console.log("index",index);

// ******************************************** join()
// The join() method returns an array as a string.
// The join() method does not change the original array.
// Any separator can be specified. The default is comma (,).
// const fruits = ["Banana", "Orange", "Apple", "Mango"];
// let text = fruits.join(" and ");

// ******************************************* shift()
//The slice() method returns selected elements in an array, as a new array.
//The slice() method selects from a given start, up to a (not inclusive) given end.
// const fruits = ["Banana", "Orange", "Apple", "Mango"];
// fruits.shift();
// console.log("fruits",fruits);


// ******************************************* some()
// The some() method checks if any array elements pass a test (provided as a function).
// The some() method executes the function once for each array element:

const ages = [3, 10, 18, 20];
ages.some(checkAdult);
function checkAdult(age) {
  return age > 18;
}
