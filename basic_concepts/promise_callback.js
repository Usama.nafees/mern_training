// ************************ following is the simple call back function example ********************************* //
// function call_back_result_displayer_one(result)
// {
//     console.log("this result is coming from function one :"+result);
// }
// function call_back_result_displayer_two(result)
// {
//     console.log("this result is coming from function two :"+result);
// }
// function calculate_result(val1,val2,callback)
// {
//     let result = val1+val2;
//     callback(result);
// }
// // calculate_result(1,2,call_back_result_displayer_two)
// calculate_result(1,2,(result)=>{
//     console.log("yupyaaaaaaaaaaaaa",result);
// });


// ************************ following is the simple Promise example ********************************* //

//  ############## this here is an example no 1

// let _promise = new Promise((resolve,reject)=>{

//     let x=1;
//     if(x==0)
//     {
//         resolve(x);
//     }else{
//         reject("there is an error");
//     }
// });
// _promise.then((val)=>{
//     console.log("this is from than :"+val);
// }).catch((error)=>{
//     console.log("this is from catch :"+error);
// });


//  ############## this here is an example no 2

// let _function = async (val)=>{
//     return new Promise((rrr,rejecttt)=>{
//         if(val == 1)
//         {
//             rrr(" ### resolveddddddddddd"+val);
//         }else
//         {
//             rejecttt(" ### rejecttttttedddddddddddd"+val);
//         }
//     });
// }
// _function(0).then((val)=>{
// console.log("from thannnnnnnnnnnn"+val);
// }).catch((errrrrrr)=>{
//     console.log("rejecttttteddddd"+errrrrrr);
// });


// ************************ async await example ********************************* //
// async makes a function return a Promise
// await makes a function wait for a Promise
// **************************  example 1 *************************************** //

// function printString(val)
// {
//     console.log(val);
// }
// async function printMyAsync(){
//     await printString("one")
//     await printString("two")
//     await printString("three")
//   };

//   printMyAsync();
// (async()=>{
//     await printMyAsync();
// })();
// **************************  example 2 *************************************** //
async function myDisplay() {
    let myPromise = new Promise(function(resolve) {
      setTimeout(function() {resolve("Testing");}, 3000);
    });
    let myPromise2 = new Promise(function(resolve) {
        resolve("Testing promise 2")
        });
    let myPromise3 = new Promise(function(resolve) {
        resolve("Testing promise 3");
      });

    console.log( myPromise);
    console.log( myPromise2);
    console.log( myPromise3);
  }
  
  myDisplay();


// ***************************************** example 3 *********************************//
