// Promise.all() resolves only when all given promises resolve, and will reject immediately if
//  any of the promises reject (or non-promises throw an error). It is useful in cases when you
//   have interdependent tasks, where it makes sense to reject immediately upon any of them rejecting.
// // const p1 = Promise.resolve(1);
// const p2 = Promise.reject('foo');
// const p3 = 3;

// Promise.all([p1, p2, p3])
//     .then((values) => console.log(values))
//     .catch((err) => console.log(err)); // 'foo'


//     const p1 = Promise.resolve(1);
// const p2 = new Promise((resolve, reject) => {
//   setTimeout(resolve, 2, 'foo');
// });
// const p3 = 3;

// Promise.all([p1, p2, p3])
//     .then((values) => console.log(values)); // [1, 'foo', 3]


    // //============================
    // Promise.allSettled() returns an array of objects that describes the outcome of each promise. 
    // Depending on whether the promises were resolved or rejected, the resulting array of objects 
    // each contains a status string. If the status is "fulfilled", then a value property is present,
    //  or if the status is "rejected", then a reason property is present. For example:

const p1 = Promise.resolve(1);
const p2 = Promise.reject('foo');
const p3 = 3;

Promise.allSettled([p1, p2, p3])
    .then((values) => console.log(values));
